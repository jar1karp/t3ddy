attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;
uniform mat3 uNormalMatrix;

uniform vec3 uLightPosition;
uniform vec3 uSkyColor;
uniform vec3 uGroundColor;

varying vec3 vDiffuseColor;

void main(void)
{
    vec3 Position = vec3(uModelViewMatrix * vec4(aVertexPosition, 1.0));
    vec3 Normal = normalize(uNormalMatrix * aVertexNormal);
    vec3 LightDirection = normalize(uLightPosition - Position);
    float CosTheta = dot(Normal, LightDirection);

    vDiffuseColor = mix(uGroundColor, uSkyColor, 0.5 + 0.5 * CosTheta);

    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 1.0);
}