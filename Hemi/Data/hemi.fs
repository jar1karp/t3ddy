varying vec3 vDiffuseColor;

void main(void)
{
    gl_FragColor = vec4(vDiffuseColor, 1.0);
}