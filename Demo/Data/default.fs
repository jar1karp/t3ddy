uniform vec3 uLightPosition;
uniform vec3 uAmbientColor;
uniform vec3 uDiffuseColor;
uniform vec3 uSpecularColor;

varying vec3 vNormal;
varying vec3 vPosition;

void main()
{
	vec3 normal = normalize(vNormal);
	vec3 lightDirection = normalize(uLightPosition - vPosition);
	vec3 reflectDirection = reflect(-lightDirection, normal);
	vec3 viewDirection = normalize(-vPosition);

	float lambertian = max(dot(lightDirection,normal), 0.0);
	float specular = 0.0;

	if (lambertian > 0.0)
	{
		float specularAngle = max(dot(reflectDirection, viewDirection), 0.0);
		specular = pow(specularAngle, 4.0);
	}

	gl_FragColor = vec4(uAmbientColor + lambertian*uDiffuseColor + specular*uSpecularColor, 1.0);
}
