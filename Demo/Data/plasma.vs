attribute vec3 aVertexPosition;
attribute vec2 aTextureCoordinate;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

varying vec2 vTextureCoordinate;
varying vec2 vPosition;

void main()
{
	gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 1.0);
	vTextureCoordinate = aTextureCoordinate;
	vPosition = vec2(aVertexPosition);
}
