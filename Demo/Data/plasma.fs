uniform sampler2D uTexture;
uniform float uTime;
uniform float uScreenWidth;
uniform float uScreenHeight;

varying vec2 vTextureCoordinate;
varying vec2 vPosition;

void main()
{
	vec2 position = vec2((gl_FragCoord.x / uScreenWidth) - 0.5, (gl_FragCoord.y / uScreenHeight) - 0.5);

	float dist_from_center = length(position);
	float angle_from_center = atan(position.y, position.x);

	float radius = dist_from_center + sin(uTime * 8.0) * 0.1 + 0.1;
	float angle = angle_from_center + uTime;

	float gradient = 0.5 / radius + sin(angle * 5.0) * 0.3;
	vec3 color = vec3(gradient, gradient / 2.0, gradient / 3.0);

	vec4 tex = texture2D(uTexture, vTextureCoordinate);

	gl_FragColor = mix(tex, vec4(color, 1.0), 0.5);
}
