#pragma once

#include "Common.h"
#include "Mat3.h"
#include "Mat4.h"
#include "Vec3.h"

namespace T3ddy
{

class Camera
{
public:
	Camera();
	Camera(const Vec3& position, const Vec3& forward, const Vec3& up);
	virtual ~Camera();
	void setPosition(const Vec3& position);
	Vec3 getPosition();
	void setDirection(const Vec3& forward);
	Vec3 getDirection();
	Mat4 getViewMatrix();
	void updateOrientation(GLfloat deltaHorizontalAngle, GLfloat deltaVerticalAngle);
	void updatePosition(const Vec3& displacement);
private:
	Vec3 position, forward, right, up;
	GLfloat horizontalAngle, verticalAngle;
};

}
