################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Camera.cpp \
../Game.cpp \
../Mat3.cpp \
../Mat4.cpp \
../Mesh.cpp \
../Shader.cpp \
../Texture2D.cpp \
../Vec2.cpp \
../Vec3.cpp 

OBJS += \
./Camera.o \
./Game.o \
./Mat3.o \
./Mat4.o \
./Mesh.o \
./Shader.o \
./Texture2D.o \
./Vec2.o \
./Vec3.o 

CPP_DEPS += \
./Camera.d \
./Game.d \
./Mat3.d \
./Mat4.d \
./Mesh.d \
./Shader.d \
./Texture2D.d \
./Vec2.d \
./Vec3.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -D_REENTRANT -I/usr/include/SDL2 -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


