#include "Game.h"

//#define FPS_COUNTER

namespace T3ddy
{

Game::Game()
: initialized(false),
  running(false),
  screen(0),
  renderer(0)
{
}

Game::~Game()
{
	if (initialized)
	{
		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(screen);
		IMG_Quit();
		SDL_Quit();
	}
}

bool Game::init(const char *title, int width, int height, bool fullscreen, bool enableSwapControl)
{
	int flags = SDL_WINDOW_OPENGL;

	if (SDL_Init(SDL_INIT_VIDEO))
		return false;

	if (!IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG))
		return false;

	if (fullscreen)
	{
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}
	else
	{
		flags |= SDL_WINDOW_RESIZABLE;
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	if (SDL_CreateWindowAndRenderer(width, height, flags, &screen, &renderer))
		return false;

	SDL_SetWindowTitle(screen, title);

	SDL_GL_SetSwapInterval(enableSwapControl ? 1 : 0);

	if (glewInit() != GLEW_OK)
		return false;

	if (!GLEW_VERSION_2_0)
		return false;

	initialized = true;

	return true;
}

void Game::run()
{
	if (!initialized)
		return;

	running = true;

	float currentTime, lastTime = timestamp(), dt;

#ifdef FPS_COUNTER
	int framecount = 0;
	float t0 = timestamp(), t1;
#endif

	while (running)
	{
		currentTime = timestamp();
		dt = currentTime - lastTime;
		update(dt);
		render();
		handleEvents();
		lastTime = currentTime;

#ifdef FPS_COUNTER
		if (++framecount % 100 == 0)
		{
			t1 = timestamp();
			printf("FPS %.2f\n", framecount / (t1 - t0));
			t0 = t1;
			framecount = 0;
		}
#endif
	}
}

float Game::timestamp()
{
	return static_cast<float>(SDL_GetTicks()) / 1000.0f;
}

void Game::update(float dt)
{
}

void Game::render()
{
}

void Game::handleKeyDown(SDL_Event *event)
{
	SDL_Keycode key = event->key.keysym.sym;

	switch (key)
	{
	case SDLK_ESCAPE:
		running = false;
		break;
	case SDLK_F11:
		toggleFullscreen();
		break;
	default:
		//printf("Key %d down\n", key);
		break;
	}
}

void Game::handleKeyUp(SDL_Event *event)
{
	SDL_Keycode key = event->key.keysym.sym;

	switch (key)
	{
	default:
		//printf("Key %d up\n", key);
		break;
	}
}

void Game::handleMouseMotion(SDL_Event *event)
{
	//printf("Mouse offset (%d, %d) relative motion (%d, %d)\n", event->motion.x, event->motion.y, event->motion.xrel, event->motion.yrel);
}

void Game::handleMouseDown(SDL_Event *event)
{
	//printf("Mouse button %d down offset (%d, %d)\n", event->button.button, event->button.x, event->button.y);
}

void Game::handleMouseUp(SDL_Event *event)
{
	//printf("Mouse button %d up offset (%d, %d)\n", event->button.button, event->button.x, event->button.y);
}

void Game::toggleFullscreen()
{
	int flags = SDL_GetWindowFlags(screen);
	if (flags & SDL_WINDOW_FULLSCREEN_DESKTOP)
	{
		flags &= ~SDL_WINDOW_FULLSCREEN_DESKTOP;
		flags |= SDL_WINDOW_RESIZABLE;
	}
	else
	{
		flags &= ~SDL_WINDOW_RESIZABLE;
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}
	SDL_SetWindowFullscreen(screen, flags);

	int width, height;
	SDL_GetWindowSize(screen, &width, &height);
	glViewport(0, 0, width, height);
}

void Game::handleResize(SDL_Event *event)
{
	int width, height;
	SDL_GetWindowSize(screen, &width, &height);
	glViewport(0, 0, width, height);
}

void Game::handleEvents()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_KEYDOWN:
			handleKeyDown(&event);
			break;
		case SDL_KEYUP:
			handleKeyUp(&event);
			break;
		case SDL_MOUSEMOTION:
			handleMouseMotion(&event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			handleMouseDown(&event);
			break;
		case SDL_MOUSEBUTTONUP:
			handleMouseUp(&event);
			break;
		case SDL_WINDOWEVENT:
			if (event.window.event == SDL_WINDOWEVENT_RESIZED)
				handleResize(&event);
			break;
		case SDL_QUIT:
			running = false;
			break;
		default:
			break;
		}
	}
}

}
