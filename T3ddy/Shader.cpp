#include <cstdio>
#include "Shader.h"

namespace T3ddy
{

Shader::Shader()
: vertexShader(0),
  fragmentShader(0),
  programObject(0)
{
}

Shader::~Shader()
{
	if (programObject)
		glDeleteProgram(programObject);
}

void Shader::setUniform(const char *name, GLfloat x)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform1f(location, x);
}

void Shader::setUniform(const char *name, GLsizei count, GLfloat *v)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform1fv(location, count, v);
}

void Shader::setUniform(const char *name, GLint x)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform1i(location, x);
}

void Shader::setUniform(const char *name, GLsizei count, GLint *v)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform1iv(location, count, v);
}

void Shader::setUniform(const char *name, const Vec2& v)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform2f(location, v.x, v.y);
}

void Shader::setUniform(const char *name, const Vec3& v)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniform3f(location, v.x, v.y, v.z);
}

void Shader::setUniform(const char *name, const Mat3& m)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniformMatrix3fv(location, 1, 0, m.ptr());
}

void Shader::setUniform(const char *name, const Mat4& m)
{
	GLuint location = glGetUniformLocation(programObject, name);
	glUniformMatrix4fv(location, 1, 0, m.ptr());
}

void Shader::setVertexAttrib(const char *name, const Vec3& v)
{
	GLuint location = glGetAttribLocation(programObject, name);
	glVertexAttrib3f(location, v.x, v.y, v.z);
}

void Shader::setVertexAttribPointer(const char *name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr)
{
	GLuint location = glGetAttribLocation(programObject, name);
	glVertexAttribPointer(location, size, type, normalized, stride, ptr);
	glEnableVertexAttribArray(location);
}

bool Shader::isValid()
{
	return programObject != 0;
}

void Shader::bind()
{
	if (programObject)
		glUseProgram(programObject);
}

void Shader::unbind()
{
	glUseProgram(0);
}

Shader *Shader::fromString(const char *vertexSource, const char *fragmentSource)
{
	GLuint vertexShader = Shader::loadShader(GL_VERTEX_SHADER, vertexSource);
	GLuint fragmentShader = Shader::loadShader(GL_FRAGMENT_SHADER, fragmentSource);

	if (!vertexShader || !fragmentShader)
		return 0;

	GLuint programObject = glCreateProgram();
	GLint linked;

	if (!programObject)
		return 0;

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	glLinkProgram (programObject);

	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1)
		{
			char *infoLog = new char[infoLen];
			glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
			fprintf(stderr, "Error linking program:\n%s\n", infoLog);
			delete[] infoLog;
		}
		glDeleteProgram (programObject);
		return 0;
	}

	Shader *shader = new Shader();
	shader->vertexShader = vertexShader;
	shader->fragmentShader = fragmentShader;
	shader->programObject = programObject;
	return shader;
}

Shader *Shader::fromFile(const char *vertexPath, const char *fragmentPath)
{
	char *vertexSource = 0;
	char *fragmentSource = 0;
	Shader *shader = 0;

	vertexSource = Shader::getFileContent(vertexPath);
	fragmentSource = Shader::getFileContent(fragmentPath);

	if (vertexSource && fragmentSource)
		shader = Shader::fromString(vertexSource, fragmentSource);

	delete[] vertexSource;
	delete[] fragmentSource;

	return shader;
}

GLuint Shader::loadShader(GLenum type, const char *source)
{
	GLuint shader;
	GLint compiled;

	shader = glCreateShader(type);

	if (!shader)
		return 0;

	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1)
		{
			char *infoLog = new char[infoLen];
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			fprintf(stderr, "Error compiling shader:\n%s\n", infoLog);
			delete[] infoLog;
		}
		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

char *Shader::getFileContent(const char *path)
{
	FILE *in = fopen(path, "r");
	char *out;
	long size;
	if (!in)
		return 0;
	fseek(in, 0, SEEK_END);
	size = ftell(in);
	out = new char[size+1]();
	fseek(in, 0, SEEK_SET);
	fread(out, 1, size, in);
	fclose(in);
	return out;
}

}
