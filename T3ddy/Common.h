#pragma once

#include <cmath>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
