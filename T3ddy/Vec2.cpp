#include "Vec2.h"

namespace T3ddy
{

GLfloat dot(const Vec2& a, const Vec2& b)
{
	return a.x*b.x + a.y*b.y;
}

Vec2 normalize(const Vec2& v)
{
	GLfloat mag = v.length();
	if (mag == 0.0f)
		return v;
	return v / mag;
}

}
