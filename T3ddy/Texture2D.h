#pragma once

#include "Common.h"

namespace T3ddy
{

class Texture2D
{
public:
	Texture2D();
	virtual ~Texture2D();
	void bind(GLenum textureUnit);
	static Texture2D *fromFile(const char *path);
private:
	GLuint textureObject;
};

}
