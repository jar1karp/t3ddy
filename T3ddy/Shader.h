#pragma once

#include "Common.h"
#include "Mat3.h"
#include "Mat4.h"
#include "Vec2.h"
#include "Vec3.h"

namespace T3ddy
{

class Shader
{
public:
	Shader();
	virtual ~Shader();

	void setUniform(const char *name, GLfloat x);
	void setUniform(const char *name, GLsizei count, GLfloat *v);
	void setUniform(const char *name, GLint x);
	void setUniform(const char *name, GLsizei count, GLint *v);
	void setUniform(const char *name, const Vec2& v);
	void setUniform(const char *name, const Vec3& v);
	void setUniform(const char *name, const Mat3& m);
	void setUniform(const char *name, const Mat4& m);

	void setVertexAttrib(const char *name, const Vec3& v);
	void setVertexAttribPointer(const char *name, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);

	bool isValid();
	void bind();
	void unbind();

	static Shader *fromString(const char *vertexSource, const char *fragmentSource);
	static Shader *fromFile(const char *vertexSource, const char *fragmentSource);

private:
	static GLuint loadShader(GLenum type, const char *source);
	static char *getFileContent(const char *path);
	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint programObject;
};

}
