#include "Mat4.h"

namespace T3ddy
{

Mat4 invert(const Mat4& m)
{
	Mat4 inv;
	GLfloat det;

	inv(0,0) = m(1,1)*m(2,2)*m(3,3) - m(1,1)*m(3,2)*m(2,3) - m(1,2)*m(2,1)*m(3,3)
					+ m(1,2)*m(3,1)*m(2,3) + m(1,3)*m(2,1)*m(3,2) - m(1,3)*m(3,1)*m(2,2);
	inv(0,1) =  -m(0,1)*m(2,2)*m(3,3) + m(0,1)*m(3,2)*m(2,3) + m(0,2)*m(2,1)*m(3,3)
					- m(0,2)*m(3,1)*m(2,3) - m(0,3)*m(2,1)*m(3,2) + m(0,3)*m(3,1)*m(2,2);
	inv(0,2) = m(0,1)*m(1,2)*m(3,3) - m(0,1)*m(3,2)*m(1,3) - m(0,2)*m(1,1)*m(3,3)
					+ m(0,2)*m(3,1)*m(1,3) + m(0,3)*m(1,1)*m(3,2) - m(0,3)*m(3,1)*m(1,2);
	inv(0,3) = -m(0,1)*m(1,2)*m(2,3) + m(0,1)*m(2,2)*m(1,3) + m(0,2)*m(1,1)*m(2,3)
					- m(0,2)*m(2,1)*m(1,3) - m(0,3)*m(1,1)*m(2,2) + m(0,3)*m(2,1)*m(1,2);
	inv(1,0) =  -m(1,0)*m(2,2)*m(3,3) + m(1,0)*m(3,2)*m(2,3) + m(1,2)*m(2,0)*m(3,3)
					- m(1,2)*m(3,0)*m(2,3) - m(1,3)*m(2,0)*m(3,2) + m(1,3)*m(3,0)*m(2,2);
	inv(1,1) = m(0,0)*m(2,2)*m(3,3) - m(0,0)*m(3,2)*m(2,3) - m(0,2)*m(2,0)*m(3,3)
					+ m(0,2)*m(3,0)*m(2,3) + m(0,3)*m(2,0)*m(3,2) - m(0,3)*m(3,0)*m(2,2);
	inv(1,2) =  -m(0,0)*m(1,2)*m(3,3) + m(0,0)*m(3,2)*m(1,3) + m(0,2)*m(1,0)*m(3,3)
					- m(0,2)*m(3,0)*m(1,3) - m(0,3)*m(1,0)*m(3,2) + m(0,3)*m(3,0)*m(1,2);
	inv(1,3) =  m(0,0)*m(1,2)*m(2,3) - m(0,0)*m(2,2)*m(1,3) - m(0,2)*m(1,0)*m(2,3)
					+ m(0,2)*m(2,0)*m(1,3) + m(0,3)*m(1,0)*m(2,2) - m(0,3)*m(2,0)*m(1,2);
	inv(2,0) = m(1,0)*m(2,1)*m(3,3) - m(1,0)*m(3,1)*m(2,3) - m(1,1)*m(2,0)*m(3,3)
					+ m(1,1)*m(3,0)*m(2,3) + m(1,3)*m(2,0)*m(3,1) - m(1,3)*m(3,0)*m(2,1);
	inv(2,1) =  -m(0,0)*m(2,1)*m(3,3) + m(0,0)*m(3,1)*m(2,3) + m(0,1)*m(2,0)*m(3,3)
					- m(0,1)*m(3,0)*m(2,3) - m(0,3)*m(2,0)*m(3,1) + m(0,3)*m(3,0)*m(2,1);
	inv(2,2) =  m(0,0)*m(1,1)*m(3,3) - m(0,0)*m(3,1)*m(1,3) - m(0,1)*m(1,0)*m(3,3)
					+ m(0,1)*m(3,0)*m(1,3) + m(0,3)*m(1,0)*m(3,1) - m(0,3)*m(3,0)*m(1,1);
	inv(2,3) = -m(0,0)*m(1,1)*m(2,3) + m(0,0)*m(2,1)*m(1,3) + m(0,1)*m(1,0)*m(2,3)
					- m(0,1)*m(2,0)*m(1,3) - m(0,3)*m(1,0)*m(2,1) + m(0,3)*m(2,0)*m(1,1);
	inv(3,0) =  -m(1,0)*m(2,1)*m(3,2) + m(1,0)*m(3,1)*m(2,2) + m(1,1)*m(2,0)*m(3,2)
					- m(1,1)*m(3,0)*m(2,2) - m(1,2)*m(2,0)*m(3,1) + m(1,2)*m(3,0)*m(2,1);
	inv(3,1) = m(0,0)*m(2,1)*m(3,2) - m(0,0)*m(3,1)*m(2,2) - m(0,1)*m(2,0)*m(3,2)
					+ m(0,1)*m(3,0)*m(2,2) + m(0,2)*m(2,0)*m(3,1) - m(0,2)*m(3,0)*m(2,1);
	inv(3,2) = -m(0,0)*m(1,1)*m(3,2) + m(0,0)*m(3,1)*m(1,2) + m(0,1)*m(1,0)*m(3,2)
					- m(0,1)*m(3,0)*m(1,2) - m(0,2)*m(1,0)*m(3,1) + m(0,2)*m(3,0)*m(1,1);
	inv(3,3) =  m(0,0)*m(1,1)*m(2,2) - m(0,0)*m(2,1)*m(1,2) - m(0,1)*m(1,0)*m(2,2)
					+ m(0,1)*m(2,0)*m(1,2) + m(0,2)*m(1,0)*m(2,1) - m(0,2)*m(2,0)*m(1,1);

	det = m(0,0)*inv(0,0) + m(1,0)*inv(0,1) + m(2,0)*inv(0,2) + m(3,0)*inv(0,3);

	if (det == 0.0f)
		return Mat4::makeIdentity();

	inv /= det;

	return inv;
}

Mat4 transpose(const Mat4& m)
{
	Mat4 t;
	for (int r = 0; r < 4; r++)
		for (int c = 0; c < 4; c++)
			t(r,c) = m(c,r);
	return t;
}

}
