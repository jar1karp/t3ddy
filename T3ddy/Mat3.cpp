#include "Mat3.h"

namespace T3ddy
{

Mat3 invert(const Mat3& m)
{
	Mat3 inv;

	GLfloat det = m(0,0)*(m(2,2)*m(1,1)-m(2,1)*m(1,2))
								- m(1,0)*(m(2,2)*m(0,1)-m(2,1)*m(0,2))
								+ m(2,0)*(m(1,2)*m(0,1)-m(1,1)*m(0,2));

	if (det == 0.0f)
		return Mat3::makeIdentity();

	inv(0,0) = m(2,2)*m(1,1)-m(2,1)*m(1,2);
	inv(1,0) = -(m(2,2)*m(1,0)-m(2,0)*m(1,2));
	inv(2,0) = m(2,1)*m(1,0)-m(2,0)*m(1,1);

	inv(0,1) = -(m(2,2)*m(0,1)-m(2,1)*m(0,2));
	inv(1,1) = m(2,2)*m(0,0)-m(2,0)*m(0,2);
	inv(2,1) = -(m(2,1)*m(0,0)-m(2,0)*m(0,1));

	inv(0,2) = m(1,2)*m(0,1)-m(1,1)*m(0,2);
	inv(1,2) = -(m(1,2)*m(0,0)-m(1,0)*m(0,2));
	inv(2,2) = m(1,1)*m(0,0)-m(1,0)*m(0,1);

	inv /= det;

	return inv;
}

Mat3 transpose(const Mat3& m)
{
	Mat3 t;
	for (int r = 0; r < 3; r++)
		for (int c = 0; c < 3; c++)
			t(r,c) = m(c,r);
	return t;
}

}
