#pragma once

#include "Common.h"
#include "Shader.h"

namespace T3ddy
{

class Mesh
{
public:
	class RenderParams
	{
	public:
		Shader *shader;
		Mat4 projectionMatrix;
		Mat4 viewMatrix;
		Vec3 lightPosition;

		/* cel */
		Vec3 ambientColor;
		Vec3 diffuseColor;
		Vec3 specularColor;
		GLfloat shininess;

		/* hemi */
		Vec3 skyColor;
		Vec3 groundColor;

		/* default */
		Vec3 solidColor;

		GLfloat time;
		Vec2 resolution;
		Vec2 mouse;

		RenderParams() : shader(0) {}
	};
	Mesh();
	Mesh(const Mesh *mesh);
	virtual ~Mesh();
	void setAngle(GLfloat angle) { this->angle = angle; needUpdate = true; }
	void setScale(const Vec3& scale) { this->scale = scale; needUpdate = true; }
	void setScale(const GLfloat uniform) { this->scale = Vec3(uniform, uniform, uniform); needUpdate = true; }
	void setRotation(const Vec3& rotation) { this->rotation = rotation; needUpdate = true; }
	void setPosition(const Vec3& position) { this->position = position; needUpdate = true; }
	void setVelocity(const Vec3& velocity) { this->velocity = velocity; needUpdate = true; }
	GLfloat getAngle() { return angle; }
	Vec3 getScale() { return scale; }
	Vec3 getRotation() { return rotation; }
	Vec3 getPosition() { return position; }
	Vec3 getVelocity() { return velocity; }
	void update(GLfloat dt);
	void render(const RenderParams& params);
	static Mesh *fromFile(const char *path);
protected:
	static std::vector<std::string> splitString(const std::string& s0, const std::string& delim);
	std::vector<GLfloat> vertices;
	std::vector<GLfloat> texCoords;
	std::vector<GLfloat> normals;
	std::vector<GLfloat> colors;
	std::vector<GLuint> indices;
	Mat4 modelMatrix;
	GLfloat angle;
	Vec3 scale;
	Vec3 rotation;
	Vec3 position;
	Vec3 velocity;
	bool needUpdate;
};

}
