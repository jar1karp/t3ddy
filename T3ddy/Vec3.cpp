#include "Vec3.h"

namespace T3ddy
{

GLfloat dot(const Vec3& a, const Vec3& b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vec3 cross(const Vec3& a, const Vec3& b)
{
	Vec3 c;

	c.x = a.y*b.z - a.z*b.y;
	c.y = a.z*b.x - a.x*b.z;
	c.z = a.x*b.y - a.y*b.x;

	return c;
}

Vec3 normalize(const Vec3& v)
{
	GLfloat mag = v.length();
	if (mag == 0.0f)
		return v;
	return v / mag;
}

}
