#include "Texture2D.h"

namespace T3ddy
{

Texture2D::Texture2D()
: textureObject(0)
{
}

Texture2D::~Texture2D()
{
	if (textureObject)
		glDeleteTextures(1, &textureObject);
}

void Texture2D::bind(GLenum textureUnit)
{
	glActiveTexture(textureUnit);
	glBindTexture(GL_TEXTURE_2D, textureObject);
}

Texture2D *Texture2D::fromFile(const char *path)
{
	Texture2D *texture = 0;
	GLuint textureObject;
	SDL_Surface *image = 0;

	GLint format;

	image = IMG_Load(path);

	if (!image)
		goto error;

	glGenTextures(1, &textureObject);

	if (!textureObject)
		goto error;

	if (image->format->BitsPerPixel == 32)
		format = GL_RGBA;
	else
		format = GL_RGB;

	glBindTexture(GL_TEXTURE_2D, textureObject);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, format, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	texture = new Texture2D();
	texture->textureObject = textureObject;

error:
	if (image)
		SDL_FreeSurface(image);
	return texture;
}

}
