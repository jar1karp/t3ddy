#pragma once

#include "Common.h"

namespace T3ddy
{

class Vec2
{
public:
	GLfloat x, y;

	Vec2() : x(0.0f), y(0.0f) {}
	Vec2(GLfloat x0, GLfloat y0) : x(x0), y(y0) {}
	Vec2(const GLfloat v[2]) : x(v[0]), y(v[1]) {}
	Vec2(const Vec2& v) : x(v.x), y(v.y) {}

	Vec2& operator=(const Vec2& v)
	{
		x = v.x;
		y = v.y;
		return *this;
	}

	Vec2& operator+=(const Vec2& v)
	{
		x += v.x;
		y += v.y;
		return *this;
	}

	Vec2& operator-=(const Vec2& v)
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	Vec2& operator-()
	{
		x = -x;
		y = -y;
		return *this;
	}

	Vec2& operator*=(GLfloat a)
	{
		x *= a;
		y *= a;
		return *this;
	}

	Vec2& operator/=(GLfloat a)
	{
		x /= a;
		y /= a;
		return *this;
	}

	friend Vec2 operator+(const Vec2& a, const Vec2& b)
	{
		Vec2 c = a;
		c += b;
		return c;
	}

	friend Vec2 operator-(const Vec2& a, const Vec2& b)
	{
		Vec2 c = a;
		c -= b;
		return c;
	}

	friend Vec2 operator*(const Vec2& a, GLfloat x)
	{
		Vec2 b = a;
		b *= x;
		return b;
	}

	friend Vec2 operator*(GLfloat x, const Vec2& a)
	{
		Vec2 b = a;
		b *= x;
		return b;
	}

	friend Vec2 operator/(const Vec2& a, GLfloat x)
	{
		Vec2 b = a;
		b /= x;
		return b;
	}

	virtual ~Vec2() {}

	GLfloat length() const
	{
		return sqrtf(x*x+y*y);
	}

	void normalize()
	{
		GLfloat mag = length();
		if (mag == 0.0f)
			return;
		x /= mag;
		y /= mag;
	}
};

GLfloat dot(const Vec2& a, const Vec2& b);
Vec2 normalize(const Vec2& v);

}
