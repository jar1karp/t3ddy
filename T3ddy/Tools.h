#pragma once

namespace T3ddy
{

class Tools
{
public:
	static inline GLfloat clamp(GLfloat x, GLfloat x0, GLfloat x1)
	{
		if (x < x0)
			return x0;
		if (x > x1)
			return x1;
		return x;
	}

	static inline GLfloat interpolate(GLfloat x, GLfloat x0, GLfloat y0, GLfloat x1, GLfloat y1)
	{
		return y0 * (x1 - x) / (x1 - x0) + y1 * (x - x0) / (x1 - x0);
	}

	static inline GLfloat smoothstep(GLfloat x, GLfloat x0, GLfloat y0, GLfloat x1, GLfloat y1)
	{
		GLfloat t = (x - x0) / (x1 - x0);
		GLfloat v = t*t*(3.0f - 2.0f*t);
		return y0*(1.0f - v) + y1*v;
	}

	static inline GLfloat smootherstep(GLfloat x, GLfloat x0, GLfloat y0, GLfloat x1, GLfloat y1)
	{
		GLfloat t = (x - x0) / (x1 - x0);
		GLfloat v = t*t*t*(t*(t*6.0f - 15.0f) + 10.0f);
		return y0*(1.0f - v) + y1*v;
	}
};

}
