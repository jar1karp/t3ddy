#pragma once

#include "Common.h"
#include "Vec3.h"

namespace T3ddy
{

class Mat3
{
public:
	// OpenGL matrices are column-major i.e. A(row,col) = A.e[row+col*3]
	GLfloat e[9];

	GLfloat& operator()(int row, int col) { return e[row+col*3]; }
	GLfloat operator()(int row, int col) const { return e[row+col*3]; }

	Mat3()
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) = 0.0f;
	}

	Mat3(const Mat3& M)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) = M(r,c);
	}

	Mat3& operator=(const Mat3& M)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) = M(r,c);
		return *this;
	}

	Mat3& operator*=(const Mat3& M)
	{
		Mat3 tmp;

		for (int r=0; r<3; r++)
		{
			tmp(r,0) = M(r,0)*(*this)(0,0) + M(r,1)*(*this)(1,0) + M(r,2)*(*this)(2,0) + M(r,3)*(*this)(3,0);
			tmp(r,1) = M(r,0)*(*this)(0,1) + M(r,1)*(*this)(1,1) + M(r,2)*(*this)(2,1) + M(r,3)*(*this)(3,1);
			tmp(r,2) = M(r,0)*(*this)(0,2) + M(r,1)*(*this)(1,2) + M(r,2)*(*this)(2,2) + M(r,3)*(*this)(3,2);
			tmp(r,3) = M(r,0)*(*this)(0,3) + M(r,1)*(*this)(1,3) + M(r,2)*(*this)(2,3) + M(r,3)*(*this)(3,3);
		}

		*this = tmp;

		return *this;
	}

	Mat3& operator*=(GLfloat x)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) *= x;
		return *this;
	}

	Mat3& operator/=(GLfloat x)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) /= x;
		return *this;
	}

	Mat3& operator+=(const Mat3& M)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) += M(r,c);
		return *this;
	}

	Mat3& operator-=(const Mat3& M)
	{
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				(*this)(r,c) -= M(r,c);
		return *this;
	}

	friend Mat3 operator*(const Mat3& A, const Mat3& B)
	{
		Mat3 C = B;
		C *= A;
		return C;
	}

	friend Mat3 operator*(const Mat3& A, GLfloat x)
	{
		Mat3 B = A;
		B *= x;
		return B;
	}

	friend Mat3 operator*(GLfloat x, const Mat3& A)
	{
		Mat3 B = A;
		B *= x;
		return B;
	}

	friend Mat3 operator/(const Mat3& A, GLfloat x)
	{
		Mat3 B = A;
		B /= x;
		return B;
	}

	friend Mat3 operator+(const Mat3& A, const Mat3& B)
	{
		Mat3 C = B;
		C += A;
		return C;
	}

	friend Mat3 operator-(const Mat3& A, const Mat3& B)
	{
		Mat3 C = B;
		C -= A;
		return C;
	}

	friend Vec3 operator*(const Mat3& A, const Vec3& X)
	{
		Vec3 Y;
		Y.x = A(0,0)*X.x + A(0,1)*X.y + A(0,2)*X.z;
		Y.y = A(1,0)*X.x + A(1,1)*X.y + A(1,2)*X.z;
		Y.z = A(2,0)*X.x + A(2,1)*X.y + A(2,2)*X.z;
		return Y;
	}

	virtual ~Mat3() {}

	const GLfloat *ptr() const { return &e[0]; }

	static Mat3 makeIdentity()
	{
		Mat3 identity;
		identity(0,0) = 1.0f;
		identity(1,1) = 1.0f;
		identity(2,2) = 1.0f;
		return identity;
	}
};

Mat3 invert(const Mat3& m);
Mat3 transpose(const Mat3& m);

}
