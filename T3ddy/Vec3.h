#pragma once

#include "Common.h"

namespace T3ddy
{

class Vec3
{
public:
	GLfloat x, y, z;

	Vec3() : x(0.0f), y(0.0f), z(0.0f) {}
	Vec3(GLfloat x0, GLfloat y0, GLfloat z0) : x(x0), y(y0), z(z0) {}
	Vec3(const GLfloat v[3]) : x(v[0]), y(v[1]), z(v[2]) {}
	Vec3(const Vec3& v) : x(v.x), y(v.y), z(v.z) {}

	static Vec3 fromRGB(GLuint rgb)
	{
		Vec3 color;
		color.x = ((rgb >> 16) & 0xFF) / 255.0;
		color.y = ((rgb >> 8) & 0xFF) / 255.0;
		color.z = ((rgb >> 0) & 0xFF) / 255.0;
		return color;
	}

	Vec3& operator=(const Vec3& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	Vec3& operator+=(const Vec3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	Vec3& operator-=(const Vec3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	Vec3& operator-()
	{
		x = -x;
		y = -y;
		z = -z;
		return *this;
	}

	Vec3& operator*=(GLfloat a)
	{
		x *= a;
		y *= a;
		z *= a;
		return *this;
	}

	Vec3& operator/=(GLfloat a)
	{
		x /= a;
		y /= a;
		z /= a;
		return *this;
	}

	friend Vec3 operator+(const Vec3& a, const Vec3& b)
	{
		Vec3 c = a;
		c += b;
		return c;
	}

	friend Vec3 operator-(const Vec3& a, const Vec3& b)
	{
		Vec3 c = a;
		c -= b;
		return c;
	}

	friend Vec3 operator*(const Vec3& a, GLfloat x)
	{
		Vec3 b = a;
		b *= x;
		return b;
	}

	friend Vec3 operator*(GLfloat x, const Vec3& a)
	{
		Vec3 b = a;
		b *= x;
		return b;
	}

	friend Vec3 operator/(const Vec3& a, GLfloat x)
	{
		Vec3 b = a;
		b /= x;
		return b;
	}

	virtual ~Vec3() {}

	GLfloat length() const
	{
		return sqrtf(x*x+y*y+z*z);
	}

	void normalize()
	{
		GLfloat mag = length();
		if (mag == 0.0f)
			return;
		x /= mag;
		y /= mag;
		z /= mag;
	}
};

GLfloat dot(const Vec3& a, const Vec3& b);
Vec3 cross(const Vec3& a, const Vec3& b);
Vec3 normalize(const Vec3& v);

}
