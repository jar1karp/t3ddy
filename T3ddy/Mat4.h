#pragma once

#include "Common.h"
#include "Mat3.h"
#include "Vec3.h"

namespace T3ddy
{

class Mat4
{
public:
	// OpenGL matrices are column-major i.e. A(row,col) = A.e[row+col*4]
	GLfloat e[16];

	GLfloat& operator()(int row, int col) { return e[row+col*4]; }
	GLfloat operator()(int row, int col) const { return e[row+col*4]; }

	Mat4()
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) = 0;
	}

	Mat4(const Mat4& M)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) = M(r,c);
	}

	Mat4& operator=(const Mat4& M)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) =  M(r,c);
		return *this;
	}

	Mat4& operator*=(const Mat4& M)
	{
		Mat4 tmp;

		for (int r=0; r<4; r++)
		{
			tmp(r,0) = M(r,0)*(*this)(0,0) + M(r,1)*(*this)(1,0) + M(r,2)*(*this)(2,0) + M(r,3)*(*this)(3,0);
			tmp(r,1) = M(r,0)*(*this)(0,1) + M(r,1)*(*this)(1,1) + M(r,2)*(*this)(2,1) + M(r,3)*(*this)(3,1);
			tmp(r,2) = M(r,0)*(*this)(0,2) + M(r,1)*(*this)(1,2) + M(r,2)*(*this)(2,2) + M(r,3)*(*this)(3,2);
			tmp(r,3) = M(r,0)*(*this)(0,3) + M(r,1)*(*this)(1,3) + M(r,2)*(*this)(2,3) + M(r,3)*(*this)(3,3);
		}

		*this = tmp;

		return *this;
	}

	Mat4& operator*=(GLfloat x)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) *= x;
		return *this;
	}

	Mat4& operator/=(GLfloat x)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) /= x;
		return *this;
	}

	Mat4& operator+=(const Mat4& M)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) += M(r,c);
		return *this;
	}

	Mat4& operator-=(const Mat4& M)
	{
		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				(*this)(r,c) -= M(r,c);
		return *this;
	}

	friend Mat4 operator*(const Mat4& A, const Mat4& B)
	{
		Mat4 C = B;
		C *= A;
		return C;
	}

	friend Mat4 operator*(const Mat4& A, GLfloat x)
	{
		Mat4 B = A;
		B *= x;
		return B;
	}

	friend Mat4 operator*(GLfloat x, const Mat4& A)
	{
		Mat4 B = A;
		B *= x;
		return B;
	}

	friend Mat4 operator/(const Mat4& A, GLfloat x)
	{
		Mat4 B = A;
		B /= x;
		return B;
	}

	friend Mat4 operator+(const Mat4& A, const Mat4& B)
	{
		Mat4 C = B;
		C += A;
		return C;
	}

	friend Mat4 operator-(const Mat4& A, const Mat4& B)
	{
		Mat4 C = B;
		C -= A;
		return C;
	}

	friend Vec3 operator*(const Mat4& A, const Vec3& X)
	{
		Vec3 Y;
		Y.x = A(0,0)*X.x + A(0,1)*X.y + A(0,2)*X.z;
		Y.y = A(1,0)*X.x + A(1,1)*X.y + A(1,2)*X.z;
		Y.z = A(2,0)*X.x + A(2,1)*X.y + A(2,2)*X.z;
		return Y;
	}

	virtual ~Mat4() {}

	const GLfloat *ptr() const { return &e[0]; }

	Mat3 getRotationMatrix() const
	{
		Mat3 R;
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 3; c++)
				R(r,c) = (*this)(r,c);
		return R;
	}

	static Mat4 makeIdentity()
	{
		Mat4 identity;
		identity(0,0) = 1.0f;
		identity(1,1) = 1.0f;
		identity(2,2) = 1.0f;
		identity(3,3) = 1.0f;
		return identity;
	}

	static Mat4 makeScale(GLfloat sx, GLfloat sy, GLfloat sz, GLfloat sw = 1.0f)
	{
		Mat4 scale = Mat4::makeIdentity();
		scale(0,0) = sx;
		scale(1,1) = sy;
		scale(2,2) = sz;
		scale(3,3) = sw;
		return scale;
	}

	static Mat4 makeScale(const Vec3& v)
	{
		return Mat4::makeScale(v.x, v.y, v.z);
	}

	static Mat4 makeTranslate(GLfloat tx, GLfloat ty, GLfloat tz)
	{
		Mat4 translate = Mat4::makeIdentity();
		translate(0,3) = tx;
		translate(1,3) = ty;
		translate(2,3) = tz;
		return translate;
	}

	static Mat4 makeTranslate(const Vec3& v)
	{
		return Mat4::makeTranslate(v.x, v.y, v.z);
	}

	static Mat4 makeRotate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z)
	{
		Mat4 rotate = Mat4::makeIdentity();

		GLfloat sinAngle, cosAngle;
		GLfloat mag = sqrtf(x * x + y * y + z * z);

		sinAngle = sinf(angle * M_PI / 180.0f);
		cosAngle = cosf(angle * M_PI / 180.0f);
		if (mag > 0.0f)
		{
			GLfloat xx, yy, zz, xy, yz, zx, xs, ys, zs;
			GLfloat oneMinusCos;

			x /= mag;
			y /= mag;
			z /= mag;

			xx = x * x;
			yy = y * y;
			zz = z * z;
			xy = x * y;
			yz = y * z;
			zx = z * x;
			xs = x * sinAngle;
			ys = y * sinAngle;
			zs = z * sinAngle;
			oneMinusCos = 1.0f - cosAngle;

			rotate(0,0) = (oneMinusCos * xx) + cosAngle;
			rotate(0,1) = (oneMinusCos * xy) - zs;
			rotate(0,2) = (oneMinusCos * zx) + ys;
			rotate(0,3) = 0.0f;

			rotate(1,0) = (oneMinusCos * xy) + zs;
			rotate(1,1) = (oneMinusCos * yy) + cosAngle;
			rotate(1,2) = (oneMinusCos * yz) - xs;
			rotate(1,3) = 0.0f;

			rotate(2,0) = (oneMinusCos * zx) - ys;
			rotate(2,1) = (oneMinusCos * yz) + xs;
			rotate(2,2) = (oneMinusCos * zz) + cosAngle;
			rotate(2,3) = 0.0f;

			rotate(3,0) = 0.0f;
			rotate(3,1) = 0.0f;
			rotate(3,2) = 0.0f;
			rotate(3,3) = 1.0f;
		}

		return rotate;
	}

	static Mat4 makeRotate(GLfloat angle, const Vec3& v)
	{
		return Mat4::makeRotate(angle, v.x, v.y, v.z);
	}

	static Mat4 makeFrustum(GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat nearZ, GLfloat farZ)
	{
		Mat4 frustum = Mat4::makeIdentity();

		GLfloat deltaX = right - left;
		GLfloat deltaY = top - bottom;
		GLfloat deltaZ = farZ - nearZ;

		if ((nearZ <= 0.0f) || (farZ <= 0.0f) ||
				(deltaX <= 0.0f) || (deltaY <= 0.0f) || (deltaZ <= 0.0f))
			return frustum;

		frustum(0,0) = 2.0f * nearZ / deltaX;
		frustum(0,1) = frustum(0,2) = frustum(0,3) = 0.0f;

		frustum(1,1) = 2.0f * nearZ / deltaY;
		frustum(1,0) = frustum(1,2) = frustum(1,3) = 0.0f;

		frustum(2,0) = (right + left) / deltaX;
		frustum(2,1) = (top + bottom) / deltaY;
		frustum(2,2) = -(nearZ + farZ) / deltaZ;
		frustum(2,3) = -1.0f;

		frustum(3,2) = -2.0f * nearZ * farZ / deltaZ;
		frustum(3,0) = frustum(3,1) = frustum(3,3) = 0.0f;

		return frustum;
	}

	static Mat4 makePerspective(GLfloat fov, GLfloat aspect, GLfloat nearZ, GLfloat farZ)
	{
		GLfloat frustumW, frustumH;

		frustumH = tanf(fov / 360.0f * M_PI) * nearZ;
		frustumW = frustumH * aspect;

		return Mat4::makeFrustum(-frustumW, frustumW, -frustumH, frustumH, nearZ, farZ);
	}

	static Mat4 makeOrtho(GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat nearZ, GLfloat farZ)
	{
		Mat4 ortho = Mat4::makeIdentity();

		GLfloat deltaX = right - left;
		GLfloat deltaY = top - bottom;
		GLfloat deltaZ = farZ - nearZ;

		if ((deltaX == 0.0f) || (deltaY == 0.0f) || (deltaZ == 0.0f))
			return ortho;

		ortho(0,0) = 2.0f / deltaX;
		ortho(0,3) = -(right + left) / deltaX;
		ortho(1,1) = 2.0f / deltaY;
		ortho(1,3) = -(top + bottom) / deltaY;
		ortho(2,2) = -2.0 / deltaZ;
		ortho(2,3) = -(farZ + nearZ) / deltaZ;
		ortho(3,3) = 1.0f;

		return ortho;
	}

	static Mat4 makeLookAt(const Vec3& eye, const Vec3& center, const Vec3& up)
	{
		Vec3 F = normalize(center - eye);
		Vec3 S = normalize(cross(F, normalize(up)));
		Vec3 U = normalize(cross(S, F));

		Mat4 lookAt = Mat4::makeIdentity();

		lookAt(0,0) = S.x; lookAt(0,1) = S.y; lookAt(0,2) = S.z;
		lookAt(1,0) = U.x; lookAt(1,1) = U.y; lookAt(1,2) = U.z;
		lookAt(2,0) = -F.x; lookAt(2,1) = -F.y; lookAt(2,2) = -F.z;

		return lookAt * Mat4::makeTranslate(-eye.x, -eye.y, -eye.z);
	}
};

Mat4 invert(const Mat4& m);
Mat4 transpose(const Mat4& m);

}
