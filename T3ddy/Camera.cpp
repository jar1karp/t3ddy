#include "Camera.h"
#include "Tools.h"

namespace T3ddy
{

Camera::Camera()
: position(Vec3(0.0f,0.0f,0.0f)),
  forward(Vec3(0.0f,0.0f,-1.0f)),
  up(Vec3(0.0f,1.0f,0.0f)),
  horizontalAngle(-90.0f),
  verticalAngle(0.0f)
{
	right = cross(forward, up);
}

Camera::Camera(const Vec3& position, const Vec3& forward, const Vec3& up)
: position(position),
  forward(forward),
  up(up),
  horizontalAngle(-90.0f),
  verticalAngle(0.0f)
{
	right = cross(forward, up);
}

Camera::~Camera()
{
}

void Camera::setPosition(const Vec3& position)
{
	this->position = position;
}

Vec3 Camera::getPosition()
{
	return position;
}

void Camera::setDirection(const Vec3& direction)
{
	Vec3 forward = normalize(direction);

	horizontalAngle = 180.0f*atan2f(forward.z, forward.x)/M_PI;
	verticalAngle = 180.0f*asinf(forward.y)/M_PI;

	right.x = cosf(horizontalAngle+M_PI/2.0f);
	right.y = 0.0f;
	right.z = sinf(horizontalAngle+M_PI/2.0f);

	up = cross(right, forward);
}

Vec3 Camera::getDirection()
{
	return forward;
}

Mat4 Camera::getViewMatrix()
{
	return Mat4::makeLookAt(position, position+forward, up);
}

void Camera::updateOrientation(GLfloat deltaHorizontalAngle, GLfloat deltaVerticalAngle)
{
	horizontalAngle += deltaHorizontalAngle;

	while (horizontalAngle < -180.0f) horizontalAngle += 360.0f;
	while (horizontalAngle > +180.0f) horizontalAngle -= 360.0f;

	verticalAngle = Tools::clamp(verticalAngle+deltaVerticalAngle, -89.0f, +89.0f);

	GLfloat H = M_PI*horizontalAngle/180.0f;
	GLfloat V = M_PI*verticalAngle/180.0f;

	forward.x = cosf(H)*cosf(V);
	forward.y = sinf(V);
	forward.z = sinf(H)*cosf(V);

	right.x = cosf(H+M_PI/2.0f);
	right.y = 0.0f;
	right.z = sinf(H+M_PI/2.0f);

	up = cross(right, forward);
}

void Camera::updatePosition(const Vec3& displacement)
{
	position += displacement.x * right + displacement.y * up + displacement.z * forward;
}

}
